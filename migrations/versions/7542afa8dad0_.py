"""empty message

Revision ID: 7542afa8dad0
Revises: 155901d48b71
Create Date: 2022-05-23 15:50:46.334630

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7542afa8dad0'
down_revision = '155901d48b71'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('radcheck_username', table_name='radcheck')
    op.drop_table('radcheck')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('radcheck',
    sa.Column('id', sa.INTEGER(), server_default=sa.text("nextval('radcheck_id_seq'::regclass)"), autoincrement=True, nullable=False),
    sa.Column('username', sa.TEXT(), server_default=sa.text("''::text"), autoincrement=False, nullable=False),
    sa.Column('attribute', sa.TEXT(), server_default=sa.text("''::text"), autoincrement=False, nullable=False),
    sa.Column('op', sa.VARCHAR(length=2), server_default=sa.text("'=='::character varying"), autoincrement=False, nullable=False),
    sa.Column('value', sa.TEXT(), server_default=sa.text("''::text"), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name='radcheck_pkey')
    )
    op.create_index('radcheck_username', 'radcheck', ['username', 'attribute'], unique=False)
    # ### end Alembic commands ###
