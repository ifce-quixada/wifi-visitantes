/*
 * Table structure for table 'raduserinfo'
 */

CREATE TABLE IF NOT EXISTS raduserinfo (
  id                      serial PRIMARY KEY,
  username_id             INT NOT NULL DEFAULT 0,
  name                    text NOT NULL DEFAULT '',
  created_at              TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at              TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
