import time 
import functools

from pycpfcnpj import cpf
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from .db import db, tables_conn 

bp = Blueprint('register', __name__, url_prefix='/user')

@bp.route('/create', methods=['GET', 'POST'])
def create():
    if request.method == 'POST':
        name = request.form['name']
        username = request.form['username']
        password = request.form['password']
        accept = request.form['accept']
        error = None
        attribute = 'Cleartext-Password'
        op = ':='
        Radcheck, Raduserinfo = tables_conn()

        if not name:
            error = 'Você deve informar seu nome completo.'
        elif not accept:
            error = 'Você deve autorizar o armazenamento dos dados.'
        elif not username:
            error = 'Nome de usuário é obrigatório. Preencha com seu CPF.'
        elif not cpf.validate(username):
            error = 'O CPF inserido não é válido.'
        elif not password:
            error = 'Senha é obrigatória.' 
        if error is None:
            # try get user with same username
            user_exists = db.session.query(Radcheck).filter_by(
                    username=username).all()
            if user_exists:
                error = 'Já existe usuário com esse CPF. \
                    Caso seja o dono, utilize a senha previamente cadastrada.'
            else:
                try:
                    user = Radcheck(
                        username=username, 
                        attribute=attribute, 
                        op=op, 
                        value=password)
                    db.session.add(user)
                    db.session.commit()
                    user_info = Raduserinfo(
                        username_id=user.id,
                        name=name)
                    db.session.add(user_info)
                    db.session.commit()
                except:
                    error = f"Erro 10041 - Por favor, informe a área técnica do Campus."
                else:
                    return redirect(url_for("register.success"))
        flash(error)

    return render_template('user/create.html')

@bp.route('/create/success', methods=['GET'])
def success():
    return render_template('user/success.html')

