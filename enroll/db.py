# import sqlite3
 
import click
from flask import current_app, g
from flask.cli import with_appcontext
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.automap import automap_base

db = SQLAlchemy()

def init_db():
    connection = db.engine.connect()
    trans = connection.begin()
    with current_app.open_resource('schema.sql') as f:
        connection.execute(f.read().decode('utf8'))
        trans.commit()
    connection.close()
 
@click.command('init-db')
@with_appcontext
def init_db_command():
    init_db()
    click.echo('Initialized the database.')
 
def init_app(app):
    # app.teardown_appcontext(close_db)
    db.init_app(app)
    app.cli.add_command(init_db_command)

def tables_conn():
    Base = automap_base()
    Base.prepare(db.engine, reflect=True)
    
    Radcheck = Base.classes.radcheck
    Raduserinfo = Base.classes.raduserinfo
    return Radcheck, Raduserinfo

