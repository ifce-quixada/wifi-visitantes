import os

from dotenv import load_dotenv
from flask import Flask, redirect, url_for
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.automap import automap_base

#from .models import db 
from . import db
from . import user 

APP_ROOT = os.path.join(os.path.dirname(__file__), '..')

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    # load environment variables
    load_dotenv(os.path.join(APP_ROOT, '.env'))

    app.config.from_mapping(
        SECRET_KEY = os.getenv("SECRET_KEY", "dev"),
        SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI", ""),
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
        VERSION = os.getenv("VERSION", "1.0.0"),
        CAPTIVE_PORTAL_URL="http://172.16.0.1:9001"
        # DATABASE=os.path.join(app.instance_path, 'enroll.sqlite'),
    ) 

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    #try:
    #    os.makedirs(app.instance_path)
    #except:
    #    pass

    @app.route('/')
    def index():
        return redirect(url_for('register.create'))

    # simple health check
    # TODO connect to database
    @app.route('/health')
    def health():
        return 'OK'


    # database related
    db.init_app(app)

    #migrate = Migrate(app, db)

    # registering application
    app.register_blueprint(user.bp)

    return app
