FROM python:3.10-alpine

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . . 
EXPOSE 5000 

CMD ["waitress-serve", "--port", "5000", "--call", "enroll:create_app"]
