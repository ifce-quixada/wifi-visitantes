# wifi-visitantes

Projeto em python (com Flask) para cadastro de usuários em tabelas para
permitir acesso à Internet.

# pré-requisitos

Antes de rodar a aplicação verifique se já está configurado um banco de dados
com o *schema* de tabelas do `freeradius`. Essa aplicação cadastra usuários
nessa tabela.

Além disso você vai precisar de um servidor, esse servidor deve ser acessível
a partir do Captive Portal do PfSense, a aplicação também precisa acessar
o Captive Portal.

# preparativos

Copie o arquivo `.env.example` para `.env` e altere os valores de acordo com
seu ambiente.

# como rodar a aplicação?

Você pode usar a ferramenta de 3 maneiras basicamente:

1. Criar um *shell script* para executar a imagem Docker;
2. `docker-compose` - com ele você pode rodar a aplicação com `docker-compose
   up`;
3. Instalar em um *cluster* Kubernetes;

A maneira utilizada no Campus Quixadá é a instalação em um Kubernetes por
meio do arquivo `.gitlab-ci.yaml`.
